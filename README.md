# Saucedemo and JsonPlaceholder Automation

This repository contains UI automation tests for the Saucedemo web application and API automation tests for the JsonPlaceholder API. The tests are implemented using [Cypress](https://www.cypress.io/), a modern end-to-end testing framework.

## Prerequisites

Before running the tests, ensure that you have the following prerequisites installed on your system:

- Node.js 
- Cypress 
- vscode

## Getting Started

To get started with the project, follow these steps:

1. Clone the repository to your local machine:

git clone https://gitlab.com/swathinagaraja/saucedemo-jsonplaceholder-automation.git

2. Install the project dependencies:

cd saucedemo-jsonplaceholder-automation
npm install
npm -i init
npm install cypress --save -dev

3. Run the tests:

npm cypress run 

or 

'npm cypress open' to run on cypress test runner

## Project Structure

The project follows the following folder structure:

- `cypress/`
- `fixtures/` - Contains test data in JSON format.
- `e2e/` - Contains UI automation tests for Saucedemo.
- `e2e/APITesting/` - Contains API automation tests for JSONPlaceHolder.
- `support/` - Contains reusable utility functions and custom commands for tests.
- `PageObjects/` - Contains structured classes representing specific pages of the Saucedemo web application, encapsulating elements and actions for better test maintainability and readability.

## Continuous Integration

This project is set up for continuous integration using GitLab CI/CD. The `.gitlab-ci.yml` file contains the CI pipeline configuration. On each commit, the tests will be automatically executed in a CI environment.

## Reporting

The test execution results are displayed in the Cypress test runner interface. Additionally, you can generate detailed HTML reports using the Cypress Allure plugin or any other preferred reporting tool.
To install Allure:
run commands
1. npm i -D @shelex/cypress-allure-plugin
2. npm install -g allure-commandline --save-dev
To generate report run the command 'npx allure generate'
--Report will be generated in the allure-reports 
Also, you can generate HTML reports using cypress-mochawesome
run the command 'npm i --save-dev cypress-mochawesome-reporter'
--Report will be generated in the `reports' folder

## Contributing

Contributions are welcome! If you find any issues or want to add new features, please submit a merge request.
