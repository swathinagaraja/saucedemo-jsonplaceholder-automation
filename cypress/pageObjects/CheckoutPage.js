class Checkout {
  verifyItemDisplayedInCheckout(itemNames) {
    cy.get(".inventory_item_name").each(($itemName, index) => {
      cy.wrap($itemName).should("have.text", itemNames[index]);
    });
  }

  verifyTotalPrice(expectedTotalPrice) {
    cy.get(".summary_info_label.summary_total_label")
      .invoke("text")
      .then((text) => {
        expect(text.trim().replace("Total: $", "")).to.equal(
          expectedTotalPrice
        );
      });
  }

  finishCheckout() {
    cy.get("#finish").click();
  }

  verifyCheckoutComplete() {
    cy.url().should("include", "/checkout-complete.html");
  }

  verifyCheckoutMessage() {
    cy.get(".complete-header").should("have.text", "Thank you for your order!");
  }
}

export default Checkout;
