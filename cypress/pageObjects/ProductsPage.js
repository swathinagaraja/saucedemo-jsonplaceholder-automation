class Products {
  constructor() {
    this.sortContainer = ".product_sort_container";
    this.itemName = ".inventory_item_name";
    this.itemPrice = ".inventory_item_price";
  }

  logout() {
    cy.get("#react-burger-menu-btn").click();
    cy.get("#logout_sidebar_link").click();
  }

  sortItems(option) {
    cy.get(this.sortContainer).select(option);
  }

  getItemNames() {
    return cy.get(this.itemName).then(($items) => {
      return $items.map((index, element) => element.textContent).get();
    });
  }

  getItemPrices() {
    return cy.get(this.itemPrice).then(($items) => {
      return $items
        .map((index, element) =>
          parseFloat(element.textContent.replace("$", ""))
        )
        .get();
    });
  }

  verifySortedByName(option) {
    this.getItemNames().then((itemNames) => {
      let sortedItemNames;
      if (option === "az") {
        sortedItemNames = [...itemNames].sort((a, b) =>
          a.localeCompare(b, "en", { sensitivity: "base" })
        );
      } else if (option === "za") {
        sortedItemNames = [...itemNames].sort((a, b) =>
          b.localeCompare(a, "en", { sensitivity: "base" })
        );
      }
      expect(itemNames).to.deep.equal(sortedItemNames);
    });
  }

  verifySortedByPrice(option) {
    this.getItemPrices().then((itemPrices) => {
      let sortedItemPrices;
      if (option === "lohi") {
        sortedItemPrices = [...itemPrices].sort((a, b) => a - b);
      } else if (option === "hilo") {
        sortedItemPrices = [...itemPrices].sort((a, b) => b - a);
      }
      expect(itemPrices).to.deep.equal(sortedItemPrices);
    });
  }
}

export default Products;
