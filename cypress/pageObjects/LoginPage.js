class Login {
  setUserName(username) {
    cy.get("#user-name").type(username);
  }

  setPassword(password) {
    cy.get("#password").type(password);
  }

  clickLogin() {
    cy.get("#login-button").click();
  }

  verifyLogin() {
    cy.get(".title").should("have.text", "Products");
  }

  verifyErrorMessage() {
    cy.get("h3[data-test='error']").should("be.visible"); // Verify the error message element is visible
    cy.get("h3[data-test='error']").should(
      "have.text",
      "Epic sadface: Username and password do not match any user in this service"
    ); // Verify the error message text
  }
}

export default Login;
