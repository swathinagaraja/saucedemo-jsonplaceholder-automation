class Cart {
  openCart() {
    cy.get(".shopping_cart_link").click();
  }

  verifyCartPage() {
    cy.url().should("include", "/cart.html");
  }

  addItemToCart(itemName) {
    cy.contains(".inventory_item_name", itemName)
      .closest(".inventory_item")
      .find(".btn_inventory")
      .click();
  }

  getCartItemCount() {
    return cy.get(".shopping_cart_badge");
  }

  goToCheckout() {
    cy.get("#checkout").click();
  }

  verifyCheckoutPage() {
    cy.url().should("include", "/checkout-step-one.html");
  }

  getItemNamesInCart() {
    return cy.get(".cart_item").then(($itemNames) => {
      return $itemNames.map((index, html) => Cypress.$(html).text()).get();
    });
  }

  getCurrentCartItemPrice(itemName) {
    // Find the item in the cart based on its name
    cy.get(".cart_item")
      .contains(itemName)
      .parent() // Move up to the cart item div
      .find(".inventory_item_price") // Find the price element in the cart item div
      .then(($price) => {
        // Extract the price text, remove the $ sign, convert it to a number and return it
        return parseFloat($price.text().replace("$", ""));
      });
  }

  continueShopping() {
    cy.get("#continue-shopping").click();
  }

  itemPresentInCart(itemName) {
    cy.contains(".cart_item", itemName).should("be.visible");
  }

  itemNotPresentInCart(itemName) {
    cy.contains(".cart_item", itemName).should("not.exist");
  }

  removeItemFromCart(itemName) {
    cy.contains(".cart_item", itemName)
      .find(".btn_secondary.cart_button")
      .click();
  }
}

export default Cart;
