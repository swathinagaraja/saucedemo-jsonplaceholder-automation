class UserDetails {
  fillUserDetails(firstName, lastName, zipCode) {
    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#postal-code").type(zipCode);
    cy.get("#continue").click();
  }
}

export default UserDetails;
