import Login from "../pageObjects/LoginPage.js";
import Products from "../pageObjects/ProductsPage.js";

describe("Sort items", () => {
  const loginPage = new Login();
  const productsPage = new Products();

  beforeEach("Login", () => {
    cy.visit("https://www.saucedemo.com/");

    cy.fixture("login").then((data) => {
      loginPage.setUserName(data.username);
      loginPage.setPassword(data.password);
      loginPage.clickLogin();
      loginPage.verifyLogin();
    });
  });

  afterEach("Logout", () => {
    productsPage.logout();
  });

  it("Sort items", () => {
    //Sort items by name (A to Z)
    productsPage.sortItems("az");
    productsPage.verifySortedByName("az");

    //Sort items by name (Z to A)
    productsPage.sortItems("za");
    productsPage.verifySortedByName("za");

    //Sort items by price (Low to High
    productsPage.sortItems("lohi");
    productsPage.verifySortedByPrice("lohi");

    //Sort items by price (High to Low)
    productsPage.sortItems("hilo");
    productsPage.verifySortedByPrice("hilo");
  });
});
