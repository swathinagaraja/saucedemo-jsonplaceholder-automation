import Login from "../pageObjects/LoginPage.js";

describe("Login Test", () => {
  const loginPage = new Login();

  beforeEach(() => {
    cy.visit("https://www.saucedemo.com/");
  });

  it("Valid login - user should be able to login", () => {
    cy.fixture("login").then((data) => {
      loginPage.setUserName(data.username);
      loginPage.setPassword(data.password);
      loginPage.clickLogin();
      loginPage.verifyLogin();
    });
  });

  it("Invalid login - should display error message", () => {
    cy.fixture("login").then((data) => {
      loginPage.setUserName(data.invalidUsername);
      loginPage.setPassword(data.invalidPassword);
      loginPage.clickLogin();
      loginPage.verifyErrorMessage();
    });
  });
});
