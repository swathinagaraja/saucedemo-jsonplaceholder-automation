import Login from "../pageObjects/LoginPage.js";
import Checkout from "../pageObjects/CheckoutPage.js";
import Cart from "../pageObjects/CartPage.js";
import UserDetails from "../pageObjects/UserDetailsPage.js";
import Products from "../pageObjects/ProductsPage.js";

const loginPage = new Login();
const cartPage = new Cart();
const checkoutPage = new Checkout();
const userDetailsPage = new UserDetails();
const productsPage = new Products();

describe("Saucedemo Test", () => {
  beforeEach("Login", () => {
    cy.visit("https://www.saucedemo.com/");

    cy.fixture("login").then((data) => {
      loginPage.setUserName(data.username);
      loginPage.setPassword(data.password);
      loginPage.clickLogin();
      loginPage.verifyLogin();
    });
  });

  it("Verify Place Order Test", () => {
    //Sort the items
    productsPage.sortItems("lohi");
    productsPage.verifySortedByPrice("lohi");

    //Add items to cart
    const itemNames = ["Sauce Labs Bike Light", "Sauce Labs Bolt T-Shirt"];
    itemNames.forEach((itemName) => {
      cartPage.addItemToCart(itemName);
    });
    cy.get(".shopping_cart_badge").should("have.text", itemNames.length);

    //Navigates to the cart page
    cartPage.openCart();
    cartPage.verifyCartPage();

    //Verify the items added to cart are displayed in the cart
    cartPage.itemPresentInCart("Sauce Labs Bike Light");
    cartPage.itemPresentInCart("Sauce Labs Bolt T-Shirt");

    //Verify removing an item from the cart
    cartPage.removeItemFromCart("Sauce Labs Bolt T-Shirt");
    cartPage.itemNotPresentInCart("Sauce Labs Bolt T-Shirt");

    //Verify adding another item to the cart updates the cart count
    cartPage.continueShopping();
    cartPage.addItemToCart("Sauce Labs Onesie");
    cy.get(".shopping_cart_badge").should("have.text", "2");
    cartPage.openCart();

    //Verify navigation to the checkout page
    cartPage.goToCheckout();
    cartPage.verifyCheckoutPage();

    //verify the items displayed in the checkout page match the items added to the cart
    cy.fixture("userDetail").then((data) => {
      userDetailsPage.fillUserDetails(
        data.firstname,
        data.lastname,
        data.zipcode
      );
    });

    let cartItems = ["Sauce Labs Bike Light", "Sauce Labs Onesie"];
    checkoutPage.verifyItemDisplayedInCheckout(cartItems);

    //verify the total price displayed in the checkout page
    checkoutPage.verifyTotalPrice("19.42");

    //verify successful checkout
    checkoutPage.finishCheckout();
    checkoutPage.verifyCheckoutComplete();
    checkoutPage.verifyCheckoutMessage();
  });
});
