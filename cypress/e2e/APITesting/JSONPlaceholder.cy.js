describe("API Testing - HTTP Requests", () => {
  const BASE_URL = "https://jsonplaceholder.typicode.com";

  it("GET Call", () => {
    cy.request("GET", `${BASE_URL}/posts/1`).its("status").should("equal", 200);
  });

  it("POST Call", () => {
    cy.request({
      method: "POST",
      url: `${BASE_URL}/posts/`,
      body: {
        title: "Test Post",
        body: "This is post call",
        userId: 1,
      },
      headers: { "Content-Type": "application/json" },
    })
      .its("status")
      .should("equal", 201);
  });

  it("POST Call using fixture", () => {
    cy.fixture("userApi").then((data) => {
      const requestBody = data;

      cy.request({
        method: "POST",
        url: `${BASE_URL}/posts/`,
        body: requestBody,
        headers: { "Content-Type": "application/json" },
      }).then((response) => {
        expect(response.status).to.eq(201);
        expect(response.body.title).to.equal(requestBody.title);
        expect(response.body.body).to.equal(requestBody.body);
        expect(response.body.userId).to.eq(requestBody.userId);
      });
    });
  });

  it("PUT Call", () => {
    cy.request({
      method: "PUT",
      url: `${BASE_URL}/posts/1`,
      body: {
        title: "Test Post - Updated",
        body: "This is post call",
        userId: 1,
        id: 1,
      },
      headers: { "Content-Type": "application/json" },
    })
      .its("status")
      .should("equal", 200);
  });

  it("DELETE Call", () => {
    cy.request({
      method: "DELETE",
      url: `${BASE_URL}/posts/1`,
    })
      .its("status")
      .should("equal", 200);
  });

  it("Getting all the posts", () => {
    cy.request({
      method: "GET",
      url: `${BASE_URL}/posts`,
    })
      .then((response) => {
        expect(response.status).to.eq(200);
        const postId = response.body[0].id;
        return postId;
      })
      .then((postId) => {
        cy.request({
          method: "GET",
          url: `${BASE_URL}/comments?postId=${postId}`,
          // qs: {postId: 'postId'}
        }).then((response) => {
          expect(response.status).to.eq(200);
          expect(response.body).to.have.length(5);
        });
      });
  });

  // GET request for a non-existent resource returns a 404 Not Found status
  it("GET - Not Found", () => {
    cy.request({ url: `${BASE_URL}/posts/9999`, failOnStatusCode: false })
      .its("status")
      .should("eq", 404);
  });
  
  // GET request that asks for the maximum amount of data the API should return
  it("GET - Maximum Data", () => {
    cy.request("GET", `${BASE_URL}/posts`)
      .its("body")
      .should("have.length", 100);
  });

  // The data returned from a GET request matches the expected data
  it("GET - Data Validation", () => {
    const expectedTitle =
      "sunt aut facere repellat provident occaecati excepturi optio reprehenderit";
    cy.request("GET", `${BASE_URL}/posts/1`)
      .its("body")
      .should("include", { title: expectedTitle });
  });
});